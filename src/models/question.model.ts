export class Question {

  public question: string;
  public options: string[];
  public correctAnswer: string;

  constructor(data?: any) {
    this.question = data.question;
    this.options = data.options;
    this.correctAnswer = data.correctAnswer;
  }
}