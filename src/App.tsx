import { ThemeProvider } from "@mui/material";
import "./App.css";
import { Stepper } from "./components/Stepper/Stepper";
import { ContextProvider } from "./context/Context";
import { mainTheme } from "./theme/main";

export const App = () => {
  return (
    <ContextProvider>
      <ThemeProvider theme={mainTheme}>
        <div className="App">
          <header className="App-header">
            <Stepper />
          </header>
        </div>
      </ThemeProvider>
    </ContextProvider>
  );
};

export default App;
