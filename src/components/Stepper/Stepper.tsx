import { useContext, useState } from "react";
import Context from "../../context/Context";
import { Step } from "../../enum/step.enum";
import { Highscores } from "../Highscores/Highscores";
import { NotFound } from "../NotFound/NotFound";
import { NameModeInputstep } from "./components/NameModeInputStep/NameModeInputStep";
import { QuestionList } from "./components/QuestionList/QuestionList";

export const Stepper = () => {
  const [currentStep, setCurrentStep] = useState<Step>(Step.FIRST);
  const { inputValue } = useContext(Context);

  const handleAction = () => {
    setCurrentStep(Step.SECOND);
  };

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter" && inputValue.length > 0) {
      handleAction();
    }
  };

  const onSubmit = (answers: any) => {
    setCurrentStep(Step.FINAL);
  };

  const steps = [
    <NameModeInputstep
      handleKeyPress={handleKeyPress}
      handleAction={handleAction}
    />,
    <QuestionList
      onSubmit={onSubmit}
    />,
    <Highscores 
    />,
  ];

  return steps[currentStep] || <NotFound />;
};
