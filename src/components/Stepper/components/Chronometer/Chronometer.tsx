import { Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { useContext, useEffect } from "react";
import Context from "../../../../context/Context";

const useStyles = makeStyles({
  time: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    minWidth: "100px"
  },
});


export default function Chronometer() {
  const classes = useStyles();
  const { running, time, incrementTime } = useContext(Context);

  useEffect(() => {
    let interval: NodeJS.Timeout;
    if (running) {
      interval = setInterval(() => {
        incrementTime(time);
      }, 10);
    }
    return () => clearInterval(interval);
  }, [running]);

  return (
    <div className={classes.time}>
      <Typography variant="h5" sx={{ color: "orange"}}>{("0" + Math.floor((time / 60000) % 60)).slice(-2)}</Typography>
      <Typography variant="h5" sx={{ color: "#f0f0f0"}}>: {("0" + Math.floor((time / 1000) % 60)).slice(-2)}</Typography>
    </div>
  )
}