import { useState, useContext } from "react";
import FormControl from "@mui/material/FormControl";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import Radio from "@mui/material/Radio";
import {
  Alert,
  Button,
  Step,
  StepContent,
  StepLabel,
  Stepper,
  Typography,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import { Question } from "../../../../models/question.model";
import Chronometer from "../Chronometer/Chronometer";
import Context from "../../../../context/Context";

const useStyles = makeStyles({
  questionsList: {
    padding: "10px 0px 50px 0px",
    "& .MuiStepLabel-label": {
      color: "#f0f0f0",
    },
  },
  selectedAnswerLabel: {
    color: "orange",
    marginLeft: "10px",
  },
  header: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
  },
  score: {
    display: "flex",
  },
});

type QuestionListProps = {
  onSubmit: ({ }) => void;
}

export const QuestionList = ({ onSubmit }: QuestionListProps) => {
  const classes = useStyles();
  const [selectedAnswers, setSelectedAnswers] = useState<any>({});
  const [activeStep, setActiveStep] = useState(0);
  const { questions, score, setScore, apiError } = useContext(Context);

  const handleAnswerChange = (questionIndex: number, answer: string) => {
    setSelectedAnswers((prevSelectedAnswers: any) => ({
      ...prevSelectedAnswers,
      [questionIndex]: answer,

    }));

    if (questionIndex !== questions.length - 1) {
      handleNext();
    }
  };

  const handleScore = (questionIndex: number, answer: string) => {
    if (answer === questions[questionIndex].correctAnswer) {
      setScore(score + 2);
    } else {
      setScore(score - 2);
    }
  }

  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  return (
    <>
      {!apiError ? (
        <div className={classes.questionsList}>
          <div className={classes.header}>
            <div className={classes.score}>
              <Typography variant="h5" sx={{ color: "#f0f0f0", paddingRight: 2 }}>Score:</Typography>
              <Typography variant="h5" sx={{ color: "orange" }}>{score}</Typography>
            </div>
            <div>
              <Typography variant="h2" sx={{ color: "#f0f0f0" }}>
                TriviAwto
              </Typography>
            </div>
            <Chronometer />
          </div>
          <Stepper activeStep={activeStep} orientation="vertical">
            {questions?.map((question: Question, index: number) => (
              <Step key={index}>
                <StepLabel>
                  {index + 1}. {question.question}
                  {selectedAnswers[index] && (
                    <span className={classes.selectedAnswerLabel}>
                      R. {selectedAnswers[index]}
                    </span>
                  )}
                </StepLabel>
                <StepContent>
                  <FormControl component="fieldset">
                    <RadioGroup
                      value={selectedAnswers[index] || ""}
                      onChange={(event) => {
                        handleAnswerChange(index, event.target.value)
                        handleScore(index, event.target.value)
                      }
                      }
                    >
                      {question?.options?.map((option: string, optionIndex: number) => (
                        <FormControlLabel
                          key={optionIndex}
                          value={option}
                          control={<Radio />}
                          label={option}
                        />
                      ))}
                    </RadioGroup>
                  </FormControl>
                  <div>
                    {index > 0 && (
                      <Button
                        onClick={handleBack}
                        sx={{ marginTop: 2, marginRight: 1, color: "#f0f0f0" }}
                      >
                        Back
                      </Button>
                    )}

                    <Button
                      variant="contained"
                      color="primary"
                      onClick={
                        index === questions.length - 1
                          ? () => onSubmit?.(selectedAnswers)
                          : handleNext
                      }
                      sx={{ marginTop: 2 }}
                    >
                      {index === questions.length - 1 ? "Submit" : "Next"}
                    </Button>
                  </div>
                </StepContent>
              </Step>
            ))}
          </Stepper>
        </div>
      ) :
        <Alert severity="error">Error al buscar la TriviAwto :( Intentalo más tarde</Alert>
      }
    </>
  );
};
