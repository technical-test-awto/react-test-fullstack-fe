import {
  Card,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  TextField,
  Tooltip,
  Typography,
} from "@mui/material";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { useContext, useState } from "react";
import Context from "../../../../context/Context";

type NameModeInputStepProps = {
  handleKeyPress: (e: React.KeyboardEvent<HTMLInputElement>)=> void;
  handleAction: () => void;
}

export const NameModeInputstep = ({
  handleKeyPress,
  handleAction,
}: NameModeInputStepProps) => {
  const {inputValue, setInputValue, difficulty, setDifficulty} = useContext(Context)
  const [buttonVisible, setButtonVisible] = useState<boolean>(false);

  const handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
    setButtonVisible(event.target.value.length > 0);
  };

  return (
    <Card sx={{ padding: "50px 80px", display: "flex", flexDirection: "column" }}>
      <Typography variant="h4">Welcome to TriviAwto!</Typography>

      <Typography variant="h6">Select the difficulty mode and enter your name to start playing</Typography>
      <FormControl sx={{ marginTop: 4, minWidth: 120 }}>
        <InputLabel id="difficulty-label">Difficulty</InputLabel>
        <Select
          labelId="difficulty-label"
          value={difficulty}
          label="Difficulty"
          onChange={(e) => setDifficulty(e.target.value)}
        >
          <MenuItem value={'easy'}>Easy</MenuItem>
          <MenuItem value={'medium'}>Medium</MenuItem>
          <MenuItem value={'hard'}>Hard</MenuItem>
          <MenuItem value={'inferno'}>Inferno</MenuItem>
        </Select>
      </FormControl>

      <TextField
        label="Name"
        value={inputValue}
        onChange={handleInput}
        onKeyPress={handleKeyPress}
        sx={{ marginTop: "35px" }}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              {buttonVisible && (
                <Tooltip title={"Start the game!"}>
                  <IconButton onClick={handleAction}>
                    <ArrowForwardIcon />
                  </IconButton>
                </Tooltip>
              )}
            </InputAdornment>
          ),
        }}
      />
    </Card>
  );
};
