import { makeStyles } from "@mui/styles";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
  Grid,
} from "@mui/material";
import { useContext } from "react";
import Context from "../../context/Context";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
    "& *": {
      textAlign: "center",
    },
  },
});

const highScores = [
  { place: 1, name: "Alice", score: 100, time: "00:45" },
  { place: 2, name: "Bob", score: 90, time: "00:48" },
  { place: 3, name: "Charlie", score: 85, time: "00:50" },
  { place: 4, name: "David", score: 80, time: "00:55" },
  { place: 5, name: "Eva", score: 75, time: "00:58" },
  { place: 6, name: "Frank", score: 70, time: "01:02" },
  { place: 7, name: "Grace", score: 65, time: "01:05" },
  { place: 8, name: "Hannah", score: 60, time: "01:10" },
  { place: 9, name: "Ian", score: 55, time: "01:15" },
  { place: 10, name: "Jack", score: 50, time: "01:20" },
];

export const Highscores = () => {
  const classes = useStyles();
  const {inputValue, score, time} = useContext(Context);

  return (
    <Grid container justifyContent="center">
      <Grid item xs={6}>
        <TableContainer component={Paper} sx={{ padding: "20px" }}>
          <Typography variant="h4" align="center" gutterBottom>
            High Scores
          </Typography>
          <Typography variant="body1" sx={{ paddingTop: "20px", paddingBottom: "20px" }}>{inputValue} your score was {score} and your time {new Date(time).toISOString().slice(11, 19)}</Typography>
          <Table className={classes.table} aria-label="high scores table">
            <TableHead>
              <TableRow>
                <TableCell></TableCell>
                <TableCell>Name</TableCell>
                <TableCell>Score</TableCell>
                <TableCell>Time</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {highScores.map((row) => (
                <TableRow key={row.place}>
                  <TableCell>{row.place}</TableCell>
                  <TableCell>{row.name}</TableCell>
                  <TableCell>{row.score}</TableCell>
                  <TableCell>{row.time}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
};
