import { Typography, Button, Box } from "@mui/material";
import SentimentVeryDissatisfiedIcon from "@mui/icons-material/SentimentVeryDissatisfied";

export const NotFound = () => {
  const handleGoBack = () => {
    window.location.reload();
  };

  return (
    <Box
      sx={{
        minHeight: "100vh",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
      }}
    >
      <Typography
        variant="h1"
        sx={{ fontFamily: "Bangers", fontSize: "7rem", color: "#f0f0f0" }}
      >
        404
      </Typography>
      <Typography
        variant="h4"
        sx={{ fontFamily: "Roboto Condensed", color: "#f0f0f0" }}
      >
        Oops! Trivia question not found.
      </Typography>
      <Typography
        variant="h5"
        sx={{ fontFamily: "Roboto Condensed", color: "#f0f0f0" }}
      >
        Seems like we don't have a question for this one!
      </Typography>
      <SentimentVeryDissatisfiedIcon
        style={{
          fontSize: 100,
          marginTop: 20,
          marginBottom: 20,
          color: "rgb(178, 115, 0)",
        }}
      />
      <Button
        variant="contained"
        color="primary"
        onClick={handleGoBack}
        sx={{ fontFamily: "Bangers", fontSize: "1.5rem", padding: "10px 30px" }}
      >
        Go Back
      </Button>
    </Box>
  );
};
