import axios from "axios";
import { createContext, useEffect, useState } from "react";
import { Question } from "../models/question.model";

interface ContextType {
  inputValue: string;
  setInputValue: (value: string) => void;
  difficulty: string;
  setDifficulty: (value: string) => void;
  questions: Question[];
  running: boolean;
  score: number;
  setScore: (value: number) => void;
  time: number;
  incrementTime: (value: number) => void;
  apiError: boolean;
}

const defaultValues: ContextType = {
  inputValue: '',
  setInputValue: () => {},
  difficulty: '',
  setDifficulty: () => {},
  questions: [],
  running: false,
  score: 0,
  setScore: () => {},
  time: 0,
  incrementTime: () => {},
  apiError: false,
}

const Context = createContext<ContextType>(defaultValues);

const ContextProvider = ({ children }: any) => {
  const [inputValue, setInputValue] = useState<string>(defaultValues.inputValue);
  const [difficulty, setDifficulty] = useState<string>(defaultValues.difficulty);
  const [questions, setQuestions] = useState<Question[]>(defaultValues.questions);
  const [running, setRunning] = useState<boolean>(defaultValues.running);
  const [score, setScore] = useState<number>(defaultValues.score);
  const [time, setTime] = useState<number>(defaultValues.time);
  const [apiError, setApiError] = useState<boolean>(defaultValues.apiError)

  const incrementTime = () => {
    setTime((time) => time + 10)
  }

  const getQuiz = async (mode: string) => {
    const url = `${process.env.REACT_APP_BACKEND_URL}/quizzes?difficulty=${mode}`;
    try {
      const { data } = await axios.get(url);
      setQuestions(
        data.map((q: any) => ({
          question: q.question,
          options: q.answers,
          correctAnswer: q.correctAnswer,
        }))
      );
      setRunning(true);
      setApiError(false);
    } catch (error: any) {
      if (error.response.status === 404 || 500) {
        setApiError(true);
      }
    }
  }

  useEffect(() => {
    getQuiz(difficulty)
  }, [difficulty])
  

  return(
    <Context.Provider value={{ 
      inputValue, 
      setInputValue, 
      difficulty, 
      setDifficulty, 
      questions, 
      running, 
      score, 
      setScore,
      time,
      incrementTime,
      apiError
      }}
    >
      {children}
    </Context.Provider>
  )
}

export { ContextProvider };

export default Context