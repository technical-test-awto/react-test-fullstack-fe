# Getting Started 
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

To start using this project you need to create a .env file and use this environment variable:

`REACT_APP_BACKEND_URL=http://localhost:3000`

then you can use the script `npm start` to run the quiz.
